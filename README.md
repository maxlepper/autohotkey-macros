# AutoHotKey Macros

The files in this project are each stand-alone macro files for use with AutoHotKey (AHK). These can be anywhere from simple key functionality replacements to small applications to automate more complex tasks.
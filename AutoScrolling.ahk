﻿; ====================================================================================================
; Function:       Macro to dynamically attach to windows and scroll contents at a controllable rate
; AHK version:    1.1.30.01 (U32)
; Script version: 1.0.00.00/2019-02-26/Max Lepper
;
; ====================================================================================================
; This software is provided 'as-is', without any express or implied warranty.
; In no event will the authors be held liable for any damages arising from the use of this software.
; This software is freeware with a Creative Commons Attribution-NonCommercial-ShareAlike License.
; ====================================================================================================

#SingleInstance Force
#NoEnv
SetWorkingDir %A_ScriptDir%
SetBatchLines -1

; Global var declaration
global timerStatus := 0
global timerVal := 1000
global scrollDir := 1

global showGUI := 1

global winX := 0
global winY := 0

; Get the coordinates of the current window
WinGetPos, winX, winY, , , A

; Gui declaration
Gui +AlwaysOnTop -border
Gui Add, Slider, ggetSpeed vSpeedSlider Range250-5000 x-1 y20 w120 h32, 2375
Gui Add, CheckBox, gboxChecked vAScheckBox x5 y3 w106 h15, Auto-Scrolling

try
    Gui Show, NoActivate x%winX% y%winY% w118 h53, Window ; Bind the GUI to the upper left corner of the active window
catch
    Gui Show, NoActivate x0 y0 w118 h53, Window ; Bind the GUI to the upper left corner of 1st monitor

; Update our initial timerVal to match our slider default value
GuiControlGet, result , , SpeedSlider,
timerVal := result

SetTimer, updateGUI, 250 ; Update our GUI position. It doesn't need to be super fast so supposedly this is less intensive than shell hooks

Return

boxChecked:
    GuiControlGet, boxChecked , , AScheckBox
        if (boxChecked = true)
        {
            SetTimer, sendKey, Off ; reset if the timer is already running
            SetTimer, sendKey, %timerVal%
        } else {
            SetTimer, sendKey, Off
        }
    return

getSpeed:
    GuiControlGet, result , , SpeedSlider,
    timerVal := result
    Goto boxChecked
    return

sendKey:
    if (scrollDir = 1) {
        Send {Down}
        ;Click, WheelDown  ; scrolls further than keyboard, left here for applications where up/down won't work
    } else {
        Send {Up}
        ;Click, WheelUp  ; scrolls further than keyboard, left here for applications where up/down won't work
    }
    Goto updateGUI
    return

updateGUI:
    if (showGUI = true)
    {
        ; Get the coordinates of the current window
        try {
            WinGetPos, winX, winY, , , A
            Gui Show, NoActivate x%winX% y%winY% w118 h53, Window ; Bind the GUI to the upper left corner of the active window
        }
        catch e {
        }
    }
    Return

GuiEscape:
GuiClose:
    ExitApp

NumpadSub::
    if ((timerVal - 100) > 250) {
        timerVal -= 100
    } else {
        timerVal:= 250
    }
    GuiControl,, SpeedSlider, %timerVal%
    Goto boxChecked
    Return

NumpadAdd::
    if ((timerVal + 100) < 5000) {
        timerVal += 100
    } else {
        timerVal:= 5000
    }
    GuiControl,, SpeedSlider, %timerVal%
    Goto boxChecked
    Return

NumpadMult::
    scrollDir *= -1
    Return

NumpadDiv::
    showGUI *= -1
    if (showGUI = -1){
        Gui, Hide
    }
    Return

F12::
NumpadEnter::
    GuiControlGet, boxChecked , , AScheckBox
    If (boxChecked = 0) {
        GuiControl,, AScheckBox, 1
        Goto boxChecked
    } else {
        GuiControl,, AScheckBox, 0
        Goto boxChecked
    }
    return

F8::
    Goto GuiClose
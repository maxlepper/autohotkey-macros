﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

; Double-click, select all, copy
z:: Send, {Click 2}^a^c
Return

; Double-click, select all, paste
x:: Send, {Click 2}^a^v
Return
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
setBatchLines -1 ; Run script at maximum speed

RAlt::
	While GetKeyState("RAlt", "P"){
        sendInput, {LButton}
	    Sleep 10 ; lowest possible increment
	}
return
